# ONLYOFFICE Workspace

An ansible role to install ONLYOFFICE Workspace using Docker Compose, see the [upstream GitHub project](https://github.com/ONLYOFFICE/Docker-CommunityServer) and the [nextcloud-server repo](httos://git.coop/webarch/nextcloud-server) which is used to deploy a development server that uses this role.

## Usage

The MySQL password are written to the `docker-compose.yml` files and the `config/mysql/docker-entrypoint-initdb.d/onlyoffice-initdb.sql` database initialization script.

## Role variables

See the [defaults/main.yml](defaults/main.yml) file for the default variables, the [vars/main.yml](vars/main.yml) file for the preset variables and the [meta/argument_specs.yml](meta/argument_specs.yml) file for the variable specification.

### workspace

The `workspace` variable is required to be defined, by default it is `false`, when it is `true` all tasks in this role will be run.

### workspace_checks

A boolean, `true` by default, set `workspace_checks` to false to skip the [tasks/checks.yml](tasks/checks.yml).

### workspace_core_machine_key

A secret string, please use Ansible Vault to update and encrypt this secret, it defaults to the upsteam insecure value, `core_secret`.

### workspace_docker_compose

The full contents of the `docker-compose.yml` file that wil be written to `/opt/Docker-CommunityServer/docker-compose.yml`.

### workspace_onlyoffice_elasticsearch_docker_tag

The Docker container version tag for the ONLYOFFICE elasticsearch container from [Docker Hub onlyoffice/elasticsearch](https://hub.docker.com/r/onlyoffice/elasticsearch).

### workspace_onlyoffice_elasticsearch_docker_tag_latest

Fail if the ONLYOFFICE Workspace elasticsearch Docker image tag is not the latest, `workspace_onlyoffice_elasticsearch_docker_tag_latest` defaults to `true`.

### workspace_git_repo_tag

The tag of the version of the [GitHub ONLYOFFICE/Docker-CommunityServer project](https://github.com/ONLYOFFICE/Docker-CommunityServer) that should be present in `/opt/Docker-CommunityServer`.

### workspace_git_repo_tag_latest

Fail if the version for the Docker-CommunityServer git repo is not the latest, `workspace_git_repo_tag_latest` defaults to `true`.

### workspace_jwt_secret

A secret string, please use Ansible Vault to update and encrypt this secret, it defaults to the upsteam, insecure value, `jwt_secret`.

### workspace_mysql_docker_tag

The MySQL container tag from [Docker Hub](https://hub.docker.com/_/mysql).

### workspace_mysql_docker_tag_latest

Fail if the version of the MySQL container is not the latest, `workspace_mysql_docker_tag_latest` defaults to `false`.

### workspace_mysql_mail_admin_password

The MySQL mail_admin password.

### workspace_mysql_onlyoffice_user_password

The MySQL onlyoffice_user password.

### workspace_mysql_root_password

The MySQL root password.

### workspace_onlyoffice_communityserver_docker_tag

The Docker container version tag for the ONLYOFFICE Community Server container from [Docker Hub onlyoffice/communityserver](https://hub.docker.com/r/onlyoffice/communityserver).

### workspace_onlyoffice_communityserver_docker_tag_latest

Fail if the Docker Hub tag for ONLYOFFICE Community Server is not the latest, `workspace_onlyoffice_communityserver_docker_tag_latest` defaults to `true`.

### workspace_onlyoffice_controlpanel_docker_tag

The Docker container version tag for the ONLYOFFICE Control Panel container from [Docker Hub onlyoffice/controlpanel](https://hub.docker.com/r/onlyoffice/controlpanel).

### workspace_onlyoffice_controlpanel_docker_tag_latest

Fail if the Docker Hub tag for ONLYOFFICE Control Panel is not the latest, `workspace_onlyoffice_controlpanel_docker_tag_latest` defaults to `true`.

### workspace_onlyoffice_ee_docker_tag

The Docker container version tag for the ONLYOFFICE DocumentServer EE container from [Docker Hub onlyoffice/documentserver-ee](https://hub.docker.com/r/onlyoffice/documentserver-ee).

### workspace_onlyoffice_ee_docker_tag_latest

Fail if the Docker Hub tag for ONLYOFFICE Workspace EE is not the latest, `workspace_onlyoffice_ee_docker_tag_latest` defaults to `true`.

### workspace_onlyoffice_mailserver_docker_tag

The Docker container version tag for the ONLYOFFICE Mail Server container from [Docker Hub onlyoffice/mailserver](https://hub.docker.com/r/onlyoffice/mailserver).

### workspace_onlyoffice_mailserver_docker_tag_latest

Fail if Docker Hub tag for ONLYOFFICE Mail Server is not the latest, `workspace_onlyoffice_mailserver_docker_tag_latest` defaults to `true`.

### workspace_secret_passwords

Enforce passwords and secrets which are different from the defaultsR, `workspace_secret_passwords` defaults to `true`.

### workspace_verify

Verify all variables starting with workspace underscore using the argument spec.

## Notes

The logs are written to sub-directories of `/var/lib/docker/volumes/`.

To login to the MySQL container:

```bash
export MYSQL_PASSWD=foo
docker exec -it onlyoffice-mysql-server mysql -uroot -p"${MYSQL_PASSWD}" -D onlyoffice
```

MySQL to get the tennent ID, former ones have the `alias` appended with `_deleted`:

```mysql
select id,name,alias from tenants_tenants;
```

MySQL to update the max file upload size:

```mysql
update tenants_quota set max_file_size=102400 where tenant='${X}';
```

MySQL to remove 2FA:

```mysql
update webstudio_settings set Data = '{"Enable":false}' where TenantID='${X}' and (ID='${Y}' or ID='${Z}');
```

After making any changes restart the `monoserver`:

```bash
docker exec -it onlyoffice-community-server systemctl restart monoserve
```

## Copyright

Copyright 2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
