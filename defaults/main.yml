# Copyright 2024 Chris Croome
#
# This file is part of the Webarchitects ONLYOFFICE Workspace Ansible role.
#
# The Webarchitects ONLYOFFICE Workspace Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects ONLYOFFICE Workspace Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects ONLYOFFICE Workspace Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
workspace: false
workspace_checks: true
workspace_verify: true

# https://github.com/ONLYOFFICE/Docker-CommunityServer/releases
workspace_git_repo_tag: v12.5.2.1848
workspace_git_repo_tag_latest: true

# https://hub.docker.com/_/mysql
workspace_mysql_docker_tag: 8.0.29
workspace_mysql_docker_tag_latest: false

# https://hub.docker.com/r/onlyoffice/communityserver
workspace_onlyoffice_communityserver_docker_tag: 12.5.2.1848
workspace_onlyoffice_communityserver_docker_tag_latest: true

# https://hub.docker.com/r/onlyoffice/controlpanel
workspace_onlyoffice_controlpanel_docker_tag: 3.5.0.516
workspace_onlyoffice_controlpanel_docker_tag_latest: true

# https://hub.docker.com/r/onlyoffice/documentserver-ee
workspace_onlyoffice_ee_docker_tag: 7.5.1.1
workspace_onlyoffice_ee_docker_tag_latest: true

# https://hub.docker.com/r/onlyoffice/elasticsearch/tags
workspace_onlyoffice_elasticsearch_docker_tag: 7.16.3
workspace_onlyoffice_elasticsearch_docker_tag_latest: true

# https://hub.docker.com/r/onlyoffice/mailserver
workspace_onlyoffice_mailserver_docker_tag: 1.6.75
workspace_onlyoffice_mailserver_docker_tag_latest: true

# Set workspace_secret_passwords to false for this role to allow the following default passwords and secrets to be used
workspace_secret_passwords: true

# Please change the following password!
workspace_mysql_root_password: my-secret-pw

# Please change the following password!
workspace_mysql_onlyoffice_user_password: onlyoffice_pass

# Please change the following password!
workspace_mysql_mail_admin_password: Isadmin123

# Please change the following!
workspace_core_machine_key: core_secret

# Please change the following!
# cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 32
workspace_jwt_secret: jwt_secret

# The following defaults are from
# https://github.com/ONLYOFFICE/Docker-CommunityServer/blob/master/docker-compose.workspace_enterprise.yml
workspace_docker_compose:
  version: '3'
  services:
    onlyoffice-mysql-server:
      container_name: onlyoffice-mysql-server
      image: "mysql:{{ workspace_mysql_docker_tag }}"
      environment:
        - MYSQL_ROOT_PASSWORD={{ workspace_mysql_root_password }}
      networks:
        - onlyoffice
      stdin_open: true
      tty: true
      restart: always
      volumes:
        - ./config/mysql/conf.d:/etc/mysql/conf.d
        - ./config/mysql/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
        - mysql_data:/var/lib/mysql
    onlyoffice-community-server:
      container_name: onlyoffice-community-server
      image: "onlyoffice/communityserver:{{ workspace_onlyoffice_communityserver_docker_tag }}"
      depends_on:
        - onlyoffice-mysql-server
        - onlyoffice-document-server
        - onlyoffice-mail-server
        - onlyoffice-elasticsearch
      environment:
        - ONLYOFFICE_CORE_MACHINEKEY={{ workspace_core_machine_key }}
        - CONTROL_PANEL_PORT_80_TCP=80
        - CONTROL_PANEL_PORT_80_TCP_ADDR=onlyoffice-control-panel
        - DOCUMENT_SERVER_PORT_80_TCP_ADDR=onlyoffice-document-server
        - DOCUMENT_SERVER_JWT_ENABLED=true
        - DOCUMENT_SERVER_JWT_SECRET={{ workspace_jwt_secret }}
        - DOCUMENT_SERVER_JWT_HEADER=AuthorizationJwt
        - MYSQL_SERVER_ROOT_PASSWORD={{ workspace_mysql_root_password }}
        - MYSQL_SERVER_DB_NAME=onlyoffice
        - MYSQL_SERVER_HOST=onlyoffice-mysql-server
        - MYSQL_SERVER_USER=onlyoffice_user
        - MYSQL_SERVER_PASS={{ workspace_mysql_onlyoffice_user_password }}
        - MAIL_SERVER_API_PORT=8081
        - MAIL_SERVER_API_HOST=onlyoffice-mail-server
        - MAIL_SERVER_DB_HOST=onlyoffice-mysql-server
        - MAIL_SERVER_DB_PORT=3306
        - MAIL_SERVER_DB_NAME=onlyoffice_mailserver
        - MAIL_SERVER_DB_USER=mail_admin
        - MAIL_SERVER_DB_PASS={{ workspace_mysql_mail_admin_password }}
        - ELASTICSEARCH_SERVER_HOST=onlyoffice-elasticsearch
        - ELASTICSEARCH_SERVER_HTTPPORT=9200
      networks:
        - onlyoffice
      ports:
        - 80:80
        - 443:443
        - 5222:5222
      stdin_open: true
      tty: true
      restart: always
      privileged: true
      cgroup: host
      volumes:
        - community_data:/var/www/onlyoffice/Data
        - community_log:/var/log/onlyoffice
        - community_letsencrypt:/etc/letsencrypt
        - document_data:/var/www/onlyoffice/DocumentServerData
        - /sys/fs/cgroup:/sys/fs/cgroup:rw
        - ./certs:/var/www/onlyoffice/Data/certs
    onlyoffice-elasticsearch:
      image: "onlyoffice/elasticsearch:{{ workspace_onlyoffice_elasticsearch_docker_tag }}"
      container_name: onlyoffice-elasticsearch
      restart: always
      environment:
        - discovery.type=single-node
        - bootstrap.memory_lock=true
        - "ES_JAVA_OPTS=-Xms2g -Xmx2g -Dlog4j2.formatMsgNoLookups=true"
        - "indices.fielddata.cache.size=30%"
        - "indices.memory.index_buffer_size=30%"
        - "ingest.geoip.downloader.enabled=false"
        - "xpack.ml.enabled=false"
      networks:
        - onlyoffice
      ulimits:
        memlock:
          soft: -1
          hard: -1
        nofile:
          soft: 65535
          hard: 65535
      volumes:
        - es_data:/usr/share/elasticsearch/data
      expose:
        - "9200"
        - "9300"
    onlyoffice-document-server:
      container_name: onlyoffice-document-server
      image: "onlyoffice/documentserver-ee:{{ workspace_onlyoffice_ee_docker_tag }}"
      stdin_open: true
      tty: true
      restart: always
      environment:
        - JWT_ENABLED=true
        - JWT_SECRET={{ workspace_jwt_secret }}
        - JWT_HEADER=AuthorizationJwt
      networks:
        - onlyoffice
      expose:
        - '80'
        - '443'
      volumes:
        - document_data:/var/www/onlyoffice/Data
        - document_log:/var/log/onlyoffice
        - ./document_fonts:/usr/share/fonts/truetype/custom
        - document_forgotten:/var/lib/onlyoffice/documentserver/App_Data/cache/files/forgotten
    onlyoffice-mail-server:
      container_name: onlyoffice-mail-server
      image: "onlyoffice/mailserver:{{ workspace_onlyoffice_mailserver_docker_tag }}"
      depends_on:
        - onlyoffice-mysql-server
      hostname: "{{ inventory_hostname }}"
      environment:
        - MYSQL_SERVER=onlyoffice-mysql-server
        - MYSQL_SERVER_PORT=3306
        - MYSQL_ROOT_USER=root
        - MYSQL_ROOT_PASSWD={{ workspace_mysql_root_password }}
        - MYSQL_SERVER_DB_NAME=onlyoffice_mailserver
      networks:
        - onlyoffice
      restart: always
      privileged: true
      ports:
        - '25:25'
        - '143:143'
        - '587:587'
      stdin_open: true
      tty: true
      expose:
        - '8081'
        - '3306'
      volumes:
        - mail_data:/var/vmail
        - mail_certs:/etc/pki/tls/mailserver
        - mail_log:/var/log
    onlyoffice-control-panel:
      container_name: onlyoffice-control-panel
      depends_on:
        - onlyoffice-document-server
        - onlyoffice-mail-server
        - onlyoffice-community-server
      image: "onlyoffice/controlpanel:{{ workspace_onlyoffice_controlpanel_docker_tag }}"
      environment:
        - ONLYOFFICE_CORE_MACHINEKEY={{ workspace_core_machine_key }}
      expose:
        - '80'
        - '443'
      restart: always
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock
        - controlpanel_data:/var/www/onlyoffice/Data
        - controlpanel_log:/var/log/onlyoffice
      networks:
        - onlyoffice
      stdin_open: true
      tty: true
  networks:
    onlyoffice:
      driver: 'bridge'
  volumes:
    mail_data:
    mail_certs:
    mail_log:
    mail_mysql:
    document_data:
    document_log:
    document_forgotten:
    community_mysql:
    community_data:
    community_log:
    community_letsencrypt:
    controlpanel_data:
    controlpanel_log:
    mysql_data:
    es_data:
...
